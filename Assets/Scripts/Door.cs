﻿using UnityEngine;
using System.Collections;

public class Door : Triggerable {

    int count = 0;

    public override void trigger()
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().enabled = false;
        count++;
        print(count);
    }

    public override void unTrigger()
    {
        count--;
        print(count);
        if (count == 0)
        {
            GetComponent<Collider>().enabled = true;
            GetComponent<Renderer>().enabled = true;
        }
    }
}
