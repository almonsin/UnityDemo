﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour
{
    // Dependencies:
    //   Transform target
    //   player named "player"

    public Transform target = null;

    void OnTriggerEnter(Collider other)
    {
        if(target && other.name=="player")
        {
            other.transform.position = target.position;
        }
    }
}
