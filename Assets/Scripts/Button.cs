﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
    // Dependencies:
    //   Triggerable target

    public Triggerable target = null;

    int count = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Portable>())
        {
            count++;

            if (target && count==1)
                target.trigger();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Portable>())
        {
            count--;
            if (target && count==0)
                target.unTrigger();
        }
    }
}
