﻿using UnityEngine;
using System.Collections;

public class Triggerable : MonoBehaviour {

    public virtual void trigger()
    {
    }

    public virtual void unTrigger()
    {
    }
}
