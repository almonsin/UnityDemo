﻿using UnityEngine;
using System.Collections;

public class FPSController : MonoBehaviour
{
    // Dependencies:
    //   child entity named "camera"
    //   input axis named "Run"

    // look direction
    public float horizontalSensitivity = 15f;
    public float verticalSensitivity = 15f;
    public float minVerticalAngle = -60f;
    public float maxVerticalAngle = 60f;
    float verticalRotation = 0f;
    Transform cameraChild = null;

    // movement
    public float speed = 6.0f;
    public float runSpeed = 12.0f;
    public float jumpSpeed = 10.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;


    void Start()
    {
        if (!Application.isEditor)
            UnityEngine.Cursor.visible = false;
        cameraChild = transform.FindChild("camera");
    }

    void updateHorizontalRotation()
    {
        transform.Rotate(0, Input.GetAxis("Mouse X") * horizontalSensitivity, 0);
    }

    void updateVerticalRotation()
    {
        verticalRotation += Input.GetAxis("Mouse Y") * verticalSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, minVerticalAngle, maxVerticalAngle);
        cameraChild.transform.localEulerAngles = new Vector3(-verticalRotation, cameraChild.transform.localEulerAngles.y, 0);
    }

    void Update()
    {
        updateHorizontalRotation();
        updateVerticalRotation();

        CharacterController controller = GetComponent<CharacterController>();

        Vector3 walkDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
        walkDir = transform.TransformDirection(walkDir);
        if (Input.GetButton("Run"))
            walkDir *= runSpeed;
        else
            walkDir *= speed;

        moveDirection.x = walkDir.x;
        moveDirection.z = walkDir.z;

        if (controller.isGrounded && Input.GetButton("Jump"))
            moveDirection.y = jumpSpeed;

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}
