﻿using UnityEngine;
using System.Collections;

public class Portable : MonoBehaviour {

    public Vector3 localPos = new Vector3(0, -0.2f, 2);

    public void onPickedUp()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        transform.localRotation = Quaternion.identity;
        transform.localPosition = localPos;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    public void onDropped()
    {
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
