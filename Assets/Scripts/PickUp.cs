﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    // Dependencies:
    //   PickUpText text in gui

    public float reach = 5.0f;

    Portable pickedUp = null;

    void Update()
    {
        RaycastHit hit;

        Collider collider = GetComponentInParent<Collider>();
        collider.enabled = false;

        Portable portable = null;
        string text = "";
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, reach))
        {
            portable = hit.collider.gameObject.GetComponent<Portable>();
            if (portable && !pickedUp)
                text = "Pick up " + portable.name;
        }
        GameObject.Find("PickUpText").GetComponent<Text>().text = text;

        if (Input.GetMouseButtonDown(0))
        {
            if (pickedUp) // drop
            {
                pickedUp.transform.parent = null;
                pickedUp.onDropped();
                pickedUp = null;
            }
            else if (portable) // pick up
            {
                portable.transform.parent = transform.parent;
                portable.onPickedUp();
                pickedUp = portable;
            }
        }
        collider.enabled = true;
    }
}
